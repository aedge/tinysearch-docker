FROM rust:latest
LABEL maintainer="Alexander Pushkov <ale@aedge.dev>"

COPY empty.json /tmp/empty.json
RUN apt-get update && \
    apt-get -y install binaryen npm && \
    rm -r /var/lib/apt/lists /var/cache/apt/archives && \
    npm install -g terser && \
    cargo install wasm-pack tinysearch && \
    # First invocation takes considerably more time, so we pre-run it:
    tinysearch /tmp/empty.json -o /tmp

ENTRYPOINT ["/usr/local/cargo/bin/tinysearch"]
